

var model = Backbone.Model.extend({title:'',id:''});
var tasks = Backbone.Collection.extend({
 model:model,
 initialize:function(){
   var arrayObject = $.jStorage.get('things-TODO-list');
   if(arrayObject && arrayObject.length > 0) {
    this.add(arrayObject);

   }    
  this.on('add',function(){
      $.jStorage.set('things-TODO-list',this.models);
  });
  this.on('remove',function(r){
      $.jStorage.set('things-TODO-list',this.models);
  });
 },
 searchNdelete:function(obj){
   result = this.findWhere(obj);
     
     update =  this.remove([result]); 
 },


});

var li = Backbone.View.extend({
     el:'#tasklist',
     collection:function(){
     var t = new tasks();
     
     return t;
     },

    render:function(e){
      var template='';
       
      _.each(this.collection().models,function(x,y,z){
       
      template += _.template($('#item').html(),x.toJSON());
      
      });

      
      $(this.el).append(template); 

      $(this.el).listview('refresh');;
        console.log("calling create");
    },
    reset:function(e){
      $(this.el).find('li').remove();

    },
events:{
'click a#remove':'remove'
},
remove:function(e){
  
  console.log(e);
  var id = parseInt($(e.currentTarget).parent().attr('id'));
   
   if(id){
            this.collection().searchNdelete({id:id});
          
     this.reset();
     this.render();     
   }

}

});


var container = Backbone.View.extend({
   //template:_.template($('#main').html(),{}),
   el:'#container',
   
   taskList:[],
   collection:function(){
    var t = new tasks();
    return t;
  },
   initialize:function(){
            this.render();
            
            this.localStorage();
            this.refresh();
   },
   refresh:function(){
       var l = new li();
       l.reset();
       l.render();
    },
   events:{
   'click input[type=button]':'addtask',
   'dblclick input[name=title]':'addtask'
   },
   localStorage:function(){
   t = this.collection();
   if(t.models && t.models.length > 0){
      this.taskList = t.models;
      
    }
    },
   addtask:function(e){
      
     
      var v = $.trim($('input#task').val());
      var t = this.collection();
      if(v.length > 0){
         //console.log(v);
         $('input#task').val('')
          var m = new model({id:t.length+1,title:v});
          t.add(m);
          this.localStorage();
          this.refresh();
       }
       else
       {
           alert("please enter a valid task");
       }       
  
   },
   render:function(e){
   var template = _.template($('#main').html());

        $(this.el).append(template);
        $(this.el).trigger("create");
       //console.log(this);
   },

});

$(function(){


var c = new container();
});
